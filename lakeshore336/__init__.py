# -*- coding: utf-8 -*-
# Author: Carlos Vigo
# Contact: carlosv@phys.ethz.ch

""" Readout, logging and control application for
the Lakeshore 336 Temperature Controller.
"""

# Local imports
from . import __project__, lakeshore336

__all__ = [
    __project__.__author__,
    __project__.__copyright__,
    __project__.__short_version__,
    __project__.__version__,
    __project__.__project_name__,
    'lakeshore336'
]
