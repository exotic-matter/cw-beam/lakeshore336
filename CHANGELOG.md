# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2020/10/29

- Fix minor database bug
- Fix heater control routine

## [0.2.0] - 2020/10/27

- Implement data logging
- Implement heater control
- Implement device locking
- Code improvement

## [0.1.0] - 2020/09/30

- First release of the **lakeshore336** package
- Installation instructions and setup

[0.1.0]: https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/tree/v0.1.0
[0.2.0]: https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/tree/v0.2.0
[0.2.1]: https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/tree/v0.2.1