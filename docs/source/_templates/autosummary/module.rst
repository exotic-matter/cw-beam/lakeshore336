{{ name | escape | underline }}

.. rubric:: Description
.. automodule:: {{ fullname }}
.. currentmodule:: {{ fullname }}

{% if classes %}
.. rubric:: Classes
.. autosummary::
    :nosignatures:
    :toctree: {{ name }}
    {% for class in classes %}
    {{ class }}
    {% endfor %}
{% endif %}

{% if functions %}
.. rubric:: Functions
.. autosummary::
    :nosignatures:
    :toctree: {{ name }}
    {% for function in functions %}
    {{ function }}
    {% endfor %}
{% endif %}

{% if exceptions %}
.. rubric:: Exceptions
.. autosummary::
    :nosignatures:
    :toctree: {{ name }}
    {% for exception in exceptions %}
    {{ exception }}
    {% endfor %}
{% endif %}