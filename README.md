# README

[![PyPI Latest Version](https://badge.fury.io/py/lakeshore336.svg)](https://badge.fury.io/py/lakeshore336)
[![pipeline status](https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/badges/master/pipeline.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/-/commits/master)
[![coverage report](https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/badges/master/coverage.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/lakeshore336/badge/?version=stable)](https://lakeshore336.readthedocs.io/en/stable/?badge=table)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## Lakeshore336 Temperature Controller Monitor

The **lakeshore336** is a monitor and control software for the Lakeshore 336 Temperature Controller. The
package includes a device driver which manages the connection and communication through Ethernet. The
application is run as a command-line.


## Authors

* [**Carlos Vigo**](mailto:carlosv@phys.ethz.ch?subject=[GitHub%-%lakeshore336]) - *Initial work* - 
[GitLab](https://gitlab.ethz.ch/carlosv)

## Contributing

Please read our [contributing policy](CONTRIBUTING.md) for details on our code of
conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.ethz.ch/exotic-matter/cw-beam/lakeshore336).

## License

This project is licensed under the [GNU GPLv3 License](LICENSE.md)

## Built With

* [PyCharm Community Edition](https://www.jetbrains.com/pycharm//) - The IDE used
* [Sphinx](https://www.sphinx-doc.org/en/master/index.html) - Documentation

## Acknowledgments

* Nobody so far
